/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.repo;

import com.mitocode.model.Signos;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author freddar
 */
public interface ISignosRepo extends JpaRepository<Signos, Integer> {
    
}
