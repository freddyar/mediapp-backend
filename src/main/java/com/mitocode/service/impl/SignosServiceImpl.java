/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.service.impl;

import com.mitocode.model.Signos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mitocode.service.ISignosService;
import com.mitocode.repo.ISignosRepo;

/**
 *
 * @author freddar
 */
@Service
public class SignosServiceImpl implements ISignosService {
    
    @Autowired
    private ISignosRepo repo;

    @Override
    public Signos registrar(Signos t) {
        return this.repo.save(t);
    }

    @Override
    public Signos modificar(Signos t) {
        return this.repo.save(t);
    }

    @Override
    public Signos leer(Integer id) {
        return this.repo.findOne(id);
    }

    @Override
    public List<Signos> listar() {
        return repo.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        repo.delete(id);
    }
    
}
