/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.controller;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Signos;
import com.mitocode.service.impl.SignosServiceImpl;
import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author freddar
 */
@RestController
@RequestMapping("/signos")
public class SignosController {

    @Autowired
    private SignosServiceImpl service;

    @GetMapping
    public ResponseEntity<List<Signos>> listar() {
        List<Signos> pacientes = service.listar();
        return new ResponseEntity<List<Signos>>(pacientes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Signos> listarPorId(@PathVariable("id") Integer idSigno) {
        Signos pac = service.leer(idSigno);
        if (pac == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + idSigno);
        }
        return new ResponseEntity<Signos>(pac, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@Valid @RequestBody Signos paciente) {
        Signos pac = service.registrar(paciente);
        // localhost:8080/pacientes/{id}
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pac.getIdSigno()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<Object> modificar(@Valid @RequestBody Signos paciente) {
        service.modificar(paciente);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer idSigno) {
        Signos pac = service.leer(idSigno);
        if (pac == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + idSigno);
        } else {
            service.eliminar(idSigno);
        }
        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
